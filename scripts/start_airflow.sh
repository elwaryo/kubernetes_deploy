#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 1

mkdir -p /mnt/wirble/prod/dags
ln -s /mnt/wirble/prod/dags /airflow/dags

mkdir -p /mnt/wirble/prod/extentions
ln -s /mnt/wirble/prod/extentions /airflow/extentions

./setup_airflow_db.sh

source airflow.environment

case "$1" in
  webserver)
    echo "Running as web server"
    airflow initdb

    airflow scheduler --daemon &
    airflow webserver -p 8080 --daemon
    ;;
  worker)
    # To give the webserver time to run initdb.
    echo "Running as worker"
    sleep 30
    export C_FORCE_ROOT='true'
    exec airflow worker
    ;;
esac
