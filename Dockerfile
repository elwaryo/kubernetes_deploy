FROM python:3.8.3-buster

# Set up Airflow's home directory and
# expose port 8080 outside of the running container
ENV AIRFLOW_HOME /airflow
EXPOSE 8080

# Install Airflow and PSQL which will
# be used for database provisioning
COPY requirements.txt /tmp/
RUN pip install --upgrade pip && \
    pip install -r /tmp/requirements.txt && \
    rm /tmp/requirements.txt && \
    apt update && apt install -y postgresql-client

RUN mkdir -p /airflow/scripts

# Add customized airflow configuration file
COPY airflow.cfg /airflow/

# Add the scripts needed to provision and run Airflow
COPY scripts/* /airflow/scripts/

ENV PYTHONPATH "${PYTHONPATH}:${AIRFLOW_HOME}"
ENTRYPOINT ["/airflow/scripts/start_airflow.sh"]
