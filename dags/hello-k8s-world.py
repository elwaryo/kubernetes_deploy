
from airflow import DAG
from datetime import timedelta

from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.utils.dates import days_ago

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=3),
}


tolerations = [
    {
        'effect': 'NoSchedule',
        'key': 'datascience-spot',
        'operator': 'Equal',
        'value': 'true'
    }
]

resources = {'request_cpu': '100m', 'request_memory': '500Mi', 'limit_cpu': '100m', 'limit_memory': '500Mi'}

DAG_ID = 'hello-k8s-world'
task_id_name = DAG_ID + '-task'

dag = DAG(DAG_ID, default_args=default_args, schedule_interval=timedelta(days=1))

KubernetesPodOperator(task_id=task_id_name,
                      name=task_id_name,
                      image='debian:buster-slim',
                      image_pull_policy="Always",
                      in_cluster=True,
                      is_delete_operator_pod=False,
                      startup_timeout_seconds=300,
                      get_logs=True,
                      hostnetwork=False,
                      service_account_name='wirbel-airflow',
                      namespace='airflow',
                      node_selectors={'datascience-spot': 'true'},
                      resources=resources,
                      tolerations=tolerations,
                      cmds=['/bin/sh'],
                      arguments=["-c", "echo 'Hello Kubernetes world!'; sleep 60; echo DONE"],
                      dag=dag)
